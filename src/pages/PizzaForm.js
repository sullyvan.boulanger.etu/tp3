import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = this.element.querySelector('form');
		form.addEventListener('submit', this.submit);
	}

	submit(event) {
		const valueForm = document
			.querySelector('form')
			.querySelector('input[name=name]').value;
		event.preventDefault();
		if (valueForm == '') {
			window.alert('Formulaire vide !');
		} else {
			window.alert('La pizza ' + valueForm + ' a été ajoutée');
		}
		console.log('Value : ' + valueForm);
	}
}
