import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import PizzaForm from './pages/PizzaForm';
import Component from './components/Component';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.navigate('/'); // affiche la liste des pizzas

const pizzaThumbnails = document.querySelectorAll('.pizzaThumbnail');
let napolitaine;
pizzaThumbnails.forEach(element => {
	if (element.textContent.toString().includes('Napolitaine')) {
		napolitaine = element.textContent;
	}
});
console.log(napolitaine);

const slogan = new Component('small', null, "les pizzas c'est la vie");
document.querySelector('.logo').innerHTML += slogan.render();

console.log(
	document.querySelector('footer').querySelectorAll('a')[1].getAttribute('href')
); //Affiche le second lien du footer

document
	.querySelector('.pizzaListLink')
	.setAttribute('class', 'pizzaListLink active');

console.log(document.querySelector('.pizzaListLink'));

document.querySelector('.newsContainer').setAttribute('style', '');
document.querySelector('.closeButton').addEventListener('click', event => {
	document
		.querySelector('.newsContainer')
		.setAttribute('style', 'display:none');
});

Router.menuElement = document.querySelector('.mainMenu');
